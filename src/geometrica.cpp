#include <iostream>
#include "geometrica.hpp"

using namespace std;

geometrica::geometrica(){
	setBase(10);
	setAltura(10);
}

geometrica::geometrica(float base, float altura){
	setBase(base);
	setAltura(altura);
}
geometrica::geometrica(int base, int altura){
	setBase((float)base);
	setAltura((float)altura);
}
float geometrica::getBase(){
	return base;
}
void geometrica::setBase(float base){
	this->base = base;
}

float geometrica::getAltura(){
	return altura;
}
void geometrica::setAltura(float altura){
	this->altura = altura;
}

float geometrica::area(){
	return getBase()*getAltura();
}
