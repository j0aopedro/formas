#include<iostream>
#include "geometrica.hpp"
#include "triangulo.hpp"

using namespace std;

int main(){
	geometrica *forma1 = new geometrica();
	triangulo forma2;

	forma1->setBase(5);
	forma1->setAltura(10);

	forma2.setBase(30);
	forma2.setAltura(20);
	cout<<"Base:\tAltura:\tArea:"<<endl;
	cout<<forma1->getBase()<<"\t"<<forma1->getAltura()<<"\t"<<forma1->area()<<endl;
	cout<<forma2.getBase()<<"\t"<<forma2.getAltura()<<"\t"<<forma2.area()<<endl;

	delete(forma1);

}
